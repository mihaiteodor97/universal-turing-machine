#pragma once
#include<string>
#include"TuringMachine.h"

class GenerateString
{
public:
	GenerateString();

public:
	std::string returnGeneratedString();

public:
	void setTuringMachine(TuringMachine &tm);

private:
	std::string generatedString;
	TuringMachine turingMachine;

};

