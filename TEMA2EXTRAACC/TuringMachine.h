#pragma once
#include<string>
#include<vector>
#include<iostream>
#include"Transition.h"

class TuringMachine
{
public:
	TuringMachine();

public:
	void addTransition(Transition t);
	void validateString(std::string inputString);
	std::string generateString();
	Transition returnByState(int currentState);

private:
	std::vector<Transition> transitions;	//first is start, last is validation
};

