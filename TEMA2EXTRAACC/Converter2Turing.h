#pragma once
#include<string>
#include"TuringMachine.h"

class Converter2Turing
{
public:
	Converter2Turing();
	Converter2Turing(std::string &s);

public:
	void setGeneratorString(std::string &s);
	TuringMachine generateTheAutomata();

private:
	std::string generatorString;
};

