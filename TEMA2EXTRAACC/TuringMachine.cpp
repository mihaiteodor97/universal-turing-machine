#include "TuringMachine.h"
#include"Base10Converter.h"

TuringMachine::TuringMachine()
{
}

void TuringMachine::addTransition(Transition t)
{
	transitions.push_back(t);
}

void TuringMachine::validateString(std::string inputString)
{
	std::cout << "Validating the input string: " << inputString << std::endl;

	int readingHead = 0;	//position in string;
	char currentCharacter;	//current ch on reading band
	int currentState = 1;	//first transition is start
	Transition currentTransition;

	while (readingHead < inputString.size() && readingHead >= 0) {

		currentCharacter = inputString.at(readingHead);
		currentTransition = returnByState(currentState);

		std::cout << "state: " << currentState << ", ch:" << currentCharacter << std::endl;

		if (currentTransition.returnCharacter() != currentCharacter) {
			std::cout << "The string was NOT validated by the current Turing Machine!\nExiting..." << std::endl;
			return;
		}

		inputString[readingHead] = currentTransition.returnNewCharacter();	//writing over the new character

		if (currentTransition.returnDirection())
			readingHead++;
		else
			readingHead--;

		currentState = currentTransition.returnNextState();
	}

	std::cout << "The string WAS validated by the current Turing Machine!" << std::endl;

}

std::string TuringMachine::generateString()
{
	bool direction;		//true = right, false = left
	char character, newCharacter;
	int currentState, nextState;

	int iDirection, iCharacter, iNewCharacter, iCurrentState, iNextState;

	Base10Converter b2c;

	std::string generatedString = "";

	for (auto trans : transitions)
	{
		direction = trans.returnDirection();
		character = trans.returnCharacter();
		newCharacter = trans.returnNewCharacter();
		currentState = trans.returnCurrentState();
		nextState = trans.returnNextState();

		iDirection = direction;
		iCurrentState = currentState;
		iNextState = (int)nextState;
		iCharacter = (int)character;
		iNewCharacter = newCharacter;

		generatedString += b2c.getBase2Value(iCurrentState);
		generatedString += "#";
		generatedString += b2c.getBase2Value(iNextState);
		generatedString += "#";
		generatedString += b2c.getBase2Value(iCharacter);
		generatedString += "#";
		generatedString += b2c.getBase2Value(iNewCharacter);
		generatedString += "#";
		generatedString += std::to_string(iDirection);
		generatedString += "#";
	}

	return generatedString;
}

Transition TuringMachine::returnByState(int currentState)
{
	for (auto trans : transitions)
		if (trans.returnCurrentState() == currentState)
			return trans;
}

