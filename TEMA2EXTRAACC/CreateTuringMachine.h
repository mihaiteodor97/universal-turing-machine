#pragma once
#include<string>
#include"TuringMachine.h"
#include"Base2Convertor.h"

class CreateTuringMachine
{
public:
	CreateTuringMachine();

public:
	void setGeneratorString(std::string s);
	TuringMachine generateTheAutomata();
	

private:
	std::string generatorString;

};

