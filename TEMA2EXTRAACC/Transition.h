#pragma once
#include<string>

class Transition
{
public:
	Transition();
	Transition(int cs, int ns, bool d, char c, char nc);

public:
	bool returnDirection();
	char returnCharacter();
	char returnNewCharacter();
	int returnCurrentState();
	int returnNextState();

private:
	bool direction;		//true = right, false = left
	char character, newCharacter;
	int currentState, nextState;
};

