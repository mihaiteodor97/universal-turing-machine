#include "Base10Converter.h"
#include<iostream>

Base10Converter::Base10Converter()
{
}

std::string Base10Converter::getBase2Value(int base10Value)
{
	int power = 1;
	std::vector<int> remainders;
	std::string base2Value = "";

	while (base10Value) {
		remainders.push_back(base10Value % 2);
		base10Value /= 2;

		if (base10Value == 1) {
			remainders.push_back(1);
			base10Value = 0;
		}
	}

	for (int i = remainders.size()- 1; i >=0 ; --i)
		base2Value += std::to_string(remainders[i]);

	return base2Value;
}

