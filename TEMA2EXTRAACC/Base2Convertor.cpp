#include "Base2Convertor.h"

Base2Convertor::Base2Convertor()
{

}

int Base2Convertor::getBase10Value(std::vector<int> base2Value)
{
	int value = 0;
	int power = 1;
	for (int i = base2Value.size() - 1; i >= 0; i--)
	{
		value += base2Value[i] * power;
		power *= 2;
	}

	return value;
}
