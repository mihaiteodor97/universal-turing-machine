#include "CreateTuringMachine.h"
#include<iostream>

CreateTuringMachine::CreateTuringMachine()
{
}

void CreateTuringMachine::setGeneratorString(std::string s)
{
	generatorString = s;
}

TuringMachine CreateTuringMachine::generateTheAutomata()
{
	TuringMachine tm;
	Base2Convertor convertor;
	std::vector<int> buffer;
	char currentCharacter;
	int step = 1;	//to keep track of the transition element I'm currently reading

	int state = 0, nextState = 0, asciiCode = 0;
	char symbol='x', nextSymbol = 'x';
	bool direction = true;

	while(!generatorString.empty())
	{
		currentCharacter = generatorString[0];
		generatorString = generatorString.substr(1);

		if (currentCharacter != '#')
		{
			if (step == 1 || step == 2 || step == 3 || step == 4) {
				buffer.push_back(currentCharacter - '0');
			}
			else {
				if (step == 5) {
					direction = currentCharacter - '0';
				}
			}
		}
		else {
			step++;
			switch (step) {
			case 2:
				state = convertor.getBase10Value(buffer);
				buffer.clear();
				break;
			case 3:
				nextState = convertor.getBase10Value(buffer);
				buffer.clear();
				break;
			case 4:
				asciiCode = convertor.getBase10Value(buffer);
				symbol = char(asciiCode);
				buffer.clear();
				break;
			case 5:
				asciiCode = convertor.getBase10Value(buffer);
				nextSymbol = char(asciiCode);
				buffer.clear();
				break;
			case 6:
				step = 1;
				Transition newTrans(state, nextState, symbol, nextSymbol, direction);
				tm.addTransition(newTrans);
				buffer.clear();
				break;
			}
		}
	}

	return tm;
}

