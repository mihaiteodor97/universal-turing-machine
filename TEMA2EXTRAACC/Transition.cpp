#include "Transition.h"

Transition::Transition()
{
}

Transition::Transition(int cs, int ns, bool d, char c, char nc)
{
	currentState = cs;
	nextState = ns;
	direction = d;
	character = c;
	newCharacter = nc;
}


bool Transition::returnDirection()
{
	return direction;
}

char Transition::returnCharacter()
{
	return character;
}

char Transition::returnNewCharacter()
{
	return newCharacter;
}

int Transition::returnCurrentState()
{
	return currentState;
}

int Transition::returnNextState()
{
	return nextState;
}




